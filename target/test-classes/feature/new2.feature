

Feature:  Login feature for the demowebshop Webapplication

  Scenario Outline: Login using multiple input
    Given launch the browser 
    When  The home page of the website is displayed
    And Click on the login
    And  Enter the "<email>" in email textbox
    And  Enter the "<password>" in password textbox
    Then click on the login button
    And  Log out and close the browser
    
    Examples: 
      | email                 | password     | 
      | lokeshbabu@gmail.com  |    Lokessh   | 
      | lokeshbabu2@gmail.com |    1234567  | 
