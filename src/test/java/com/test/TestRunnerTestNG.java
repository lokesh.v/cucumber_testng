package com.test;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
 
@CucumberOptions(tags = "", features = "src/test/resources/feature/new2.feature", glue = "com.test",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)

public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}
