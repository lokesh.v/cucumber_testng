package com.test;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinition1 {
	public static WebDriver driver;
	@Given("launch the browser")
	public void launch_the_browser() {
		driver=new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	
	}

	@When("The home page of the website is displayed")
	public void the_home_page_of_the_website_is_displayed() {
		System.out.println("the home page of the website is displayed");

	}

	@When("Click on the login")
	public void click_on_the_login() {
		System.out.println("click on the login");
		driver.findElement(By.linkText("Log in")).click();

	}

	@When("Enter the {string} in email textbox")
	public void enter_the_in_email_textbox(String string) {
		System.out.println("enter the username in email textbox");
		driver.findElement(By.id("Email")).sendKeys(string);

	}

	@When("Enter the {string} in password textbox")
	public void enter_the_in_password_textbox(String string) {
		   System.out.println("enter the userpassword in password textbox");	
			  driver.findElement(By.id("Password")).sendKeys(string);

	}

	@Then("click on the login button")
	public void click_on_the_login_button() {
		System.out.println("click on the login button");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

	}

	@Then("Log out and close the browser")
	public void log_out_and_close_the_browser() {
		System.out.println("Log out");
	     driver.findElement(By.linkText("Log out")).click();
		 driver.close();

}


}
